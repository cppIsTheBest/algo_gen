#include <iostream>
#include <time.h>
#include "Population.hpp"

Population::Population(int size, sf::ContextSettings const& guiSettings) :
    _gui(*this, _battle, sf::VideoMode(1000, 700), "Algos_gen",
            sf::Style::Titlebar | sf::Style::Close, guiSettings),
    _nbGenerations(0),
    _nbBattles(0)
{
    // set seed for all the random numbers generations
    srand(time(NULL));

    // instanciation of creatures
    for (int i(0); i<size; i++)
        _creatures.push_back(new Creature(20));

    // set-up of the gui
    _gui.setVerticalSyncEnabled(true);
}

Population::~Population() {
    // delete creatures
    for (auto it(_creatures.begin()); it!=_creatures.end(); it++)
        delete *it;
}

void Population::evaluation() {
    _nbGenerations ++;
    _nbBattles = 0;

    for (auto it(_creatures.begin()); it != _creatures.end(); it++) {
        for (auto rit(_creatures.rbegin()); !equal(it, rit); rit++) {
            int ID1( (*it)->getGlobalID() );
            int ID2( (*rit)->getGlobalID() );
            if (ID1 != ID2) {
                // prepare battle
                _battle.init(*it, *rit);
                _gui.prepareForBattle(_battle);
                _nbBattles ++;

                // execute a fight between two creatures
                while (!_battle.fightEnded() && _gui.isOpen()) {
                    sf::Event event;
                    while (_gui.pollEvent(event)) {
                        if (event.type == sf::Event::Closed) {
                            _gui.close();
                            std::cout <<"[window closed properly]" << std::endl;
                        }
                    }

                    _battle.iterate(_gui.getViewManager());
                    _gui.display();
                }

                // save information for ranking
                int HP1( (*it)->getHP() );
                int HP2( (*rit)->getHP() );
                if (HP1 > 0.0)
                    _ranks.addVictory(ID1, HP1);
                else
                    _ranks.addVictory(ID2, HP2);
                _ranks.addHPLeft(ID1, HP1);
                _ranks.addHPLeft(ID2, HP2);
            }

            if (!_gui.isOpen())
                break;
        }
        if (!_gui.isOpen())
            break;
    }

    // compute creature's ranks
    _ranks.reset();
    _ranks.computeAllRanks();
}

void Population::creationOfNewCreatures() {
     
}

void Population::update() {
}

bool Population::windowOpen() {
    return _gui.isOpen();
}

bool Population::equal(std::list<Creature*>::iterator & it,
                       std::list<Creature*>::reverse_iterator & rit) {
    int distIT( std::distance(_creatures.begin(), it) );
    int distRIT( std::distance(rit, _creatures.rend()) - 1);
    return distIT == distRIT;
}

