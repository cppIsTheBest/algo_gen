#include <iostream>
#include "Battle.hpp"

Battle::Battle() :
    _nbIterations(0),
    _nbIterationsMax(300)
{}

void Battle::init(Creature* creature1, Creature* creature2) {
    _creature1 = creature1;
    _creature2 = creature2;
    _creature1->prepareForBattle();
    _creature2->prepareForBattle();
    _creature1->setPosition(100, 100);
    _creature2->setPosition(400, 250);
    _nbIterations = 0;
}

void Battle::iterate(ViewManager & viewManager) {
    /* /!\ when VerticalSync is enabled (see set up of gui),
     * minimum is 16ms (60 fps) */
//    if (_clock.getElapsedTime() >= sf::milliseconds(16)) {
        _clock.restart();
        act();
        resolveActions(viewManager);
        _nbIterations ++;
//    }
}

bool Battle::fightEnded() const {
    return  _creature1->getHP() == 0.0 || _creature2->getHP() == 0.0
            || _nbIterations >= _nbIterationsMax;
}

void Battle::act() {
    _actions1 = _creature1->getActions(*this);
    _actions2 = _creature2->getActions(*this);
}

void Battle::resolveActions(ViewManager & viewManager) {
    // set up future pos of creatures
    _creature1->assignCurrent2FuturePos();
    _creature2->assignCurrent2FuturePos();

    for (unsigned int i(0); i<_actions1->size(); i++)
        (*_actions1)[i]->act(viewManager, *_creature1, *_creature2);

    for (unsigned int i(0); i<_actions2->size(); i++)
        (*_actions2)[i]->act(viewManager, *_creature2, *_creature1);

    // future pos becomes current pos
    _creature1->assignFuture2CurrentPos();
    _creature2->assignFuture2CurrentPos();
}

