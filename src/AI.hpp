#ifndef AI_H_INCLUDED
#define AI_H_INCLUDED

#include <memory>
#include "Node.hpp"

/** Represent the AI (flowchart) of a creature
 */
class AI {
    public:
        AI(Node* aiRoot);

        /* returns the actions selected by the flowchart,
           given the current state of the game */
        std::vector<Action*> & getActions(Battle const& simulation);
        std::unique_ptr<Node> & getRoot() { return _aiRoot; };
        std::string toString();

    private:
        std::unique_ptr<Node> _aiRoot;
        std::vector<Action*> _actions;
};

#endif //AI_H_INCLUDED

