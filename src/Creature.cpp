#include <cmath>
#include <algorithm>
#include <iterator>
#include <iostream>
#include "Creature.hpp"
#include "ActionNode.hpp"
#include "ConditionNode.hpp"
#include "conditions/ConditionDistance.hpp"
#include "actions/ActionApproach.hpp"
#include "actions/ActionAttack.hpp"
#include "actions/ActionMoveAway.hpp"
#include "actions/ActionNop.hpp"
#include "aiModifier.hpp"

int Creature::_nbCreatureCreated = 0;

Creature::Creature(float radius) :
    _globalID(_nbCreatureCreated),
    _radius(radius),
    _HP(_HPMax),
    _energy(_energyMax)
{
    _nbCreatureCreated ++;

    unsigned int nbActuators(3);
    float angle(0), offset(2*PI/nbActuators);
    for (unsigned int i(0); i<nbActuators; i++) {
        _actuators.push_back(Actuator(angle, *this));
        angle += offset;
    }
    _ai = new AI(new ActionNode(new ActionNop()));
    for (int i(0); i<15; i++)
        aiModifier::randomMutation(*_ai);
    std::cout << "----------------------------------" << std::endl;
    std::cout << _ai->toString() << std::endl;
    std::cout << "----------------------------------\n\n\n" << std::endl;
}

Creature::~Creature() {
    delete _ai;
}

void Creature::prepareForBattle() {
    _HP = _HPMax;
    _energy = _energyMax;
}

void Creature::setPosition(float x, float y) {
    _pos.resetCoordinates(x, y);
    for (unsigned int i(0); i<_actuators.size(); i++) {
        _actuators[i].resetCoordinatesAround(x, y, _radius);
    }
}

void Creature::move(float x, float y) {
    _pos.move(x, y);
    for (unsigned int i(0); i<_actuators.size(); i++) {
        _actuators[i].resetCoordinatesAround(_pos.getX(), _pos.getY(),
                                             _radius);
    }
}

void Creature::moveFuture(float x, float y) {
    _futurePos.move(x, y);
} 

void Creature::assignFuture2CurrentPos() {
    setPosition(_futurePos.getX(), _futurePos.getY());
}

void Creature::assignCurrent2FuturePos() {
    _futurePos = _pos;
}

std::vector<Action*> * Creature::getActions(Battle const& simulation) {
    return &_ai->getActions(simulation);
}

void Creature::takeDamage(float damage) {
    _HP -= damage;
    if (_HP < 0)
        _HP = 0;
}

