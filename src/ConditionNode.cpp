#include <memory>
#include "ConditionNode.hpp"
#include "Battle.hpp"
#include "smallFcts.hpp"
#include "ActionNode.hpp"

ConditionNode::ConditionNode(Condition* condition, Node* ifNode) : Node(),
    _condition(condition),
    _ifNode(ifNode)
{}

void ConditionNode::updateActions(std::vector<Action*> & actions,
                                  Battle const& simulation) {
    if (_condition) {
        if (_condition->evaluate(simulation)) {
            if (_ifNode) {
                _ifNode->updateActions(actions, simulation);
            }
        }
        else if (_elseNode) {
            _elseNode->updateActions(actions, simulation);
        }
    }
}

void ConditionNode::randomUpdate() {
    _condition->randomUpdate();
}

void ConditionNode::insertNode(Node* n) {
    int r = rand() % 2;
    if (r == 0 || !_elseNode) {
        // insert the node before the if node
        if (ConditionNode* cn = dynamic_cast<ConditionNode*>(n)) {
            cn->setIfNode(_ifNode.release());
            _ifNode.reset(n);
        }
        else if(ActionNode* an = dynamic_cast<ActionNode*>(n)) {
            an->setNext(_ifNode.release());
            _ifNode.reset(n);
        }
    }
    else {
        // insert the node before the else node
        if (ConditionNode* cn = dynamic_cast<ConditionNode*>(n)) {
            cn->setElseNode(_elseNode.release());
            _elseNode.reset(n);
        }
        else if(ActionNode* an = dynamic_cast<ActionNode*>(n)) {
            an->setNext(_elseNode.release());
            _elseNode.reset(n);
        }
    }
}

int ConditionNode::countNodes() {
    int nodesNumber(0);
    if (_ifNode)
        nodesNumber += _ifNode->countNodes() + 1;
    if (_elseNode)
        nodesNumber += _elseNode->countNodes() + 1;
    return nodesNumber;
}

std::unique_ptr<Node> * ConditionNode::getNodeDepthFirstSearch(
                                    int & currentNumber, int nodeNumber) {
    if (_ifNode) {
        if (currentNumber + 1 == nodeNumber) {
            return &(_ifNode);
        } else {
            currentNumber ++;
            std::unique_ptr<Node> * node( _ifNode->getNodeDepthFirstSearch(
                                            currentNumber, nodeNumber) );
            if (node != nullptr) {
                return node;
            } else {
                if (_elseNode) {
                    currentNumber ++;
                    return _elseNode->getNodeDepthFirstSearch(
                                                currentNumber, nodeNumber);
                } else {
                    return nullptr;
                }
            }
        }
    } else {
        return nullptr;
    }
}

std::string ConditionNode::toString(int indentLevel) {
    if (_ifNode) {
        std::string str = smallFcts::indent(indentLevel)
              + "if (" + _condition->toString(0) + ") {\n"
              + _ifNode->toString(indentLevel+1) + "\n"
              + smallFcts::indent(indentLevel) + "}";
        if (_elseNode) {
            str += "\n" + smallFcts::indent(indentLevel) + " else {\n"
                + _elseNode->toString(indentLevel+1) + "}";
        }
        return str;
    } else {
        return std::string();
    }
}

void ConditionNode::setIfNode(Node* n) {
    _ifNode.reset(n);
}

void ConditionNode::setElseNode(Node* n) {
    _ifNode.reset(n);
}

