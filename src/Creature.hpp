#ifndef CREATURE_H_INCLUDED
#define CREATURE_H_INCLUDED

#include <vector>
#include "AI.hpp"
#include "Position.hpp"
#include "Actuator.hpp"

/** Defines a creature that has a 2D body whith actuators and an flowchart AI
 */
class Creature {
    public:
        Creature(float radius);
        ~Creature();

        /* re-initialise attributes of the creature to prepare it for the battle
         */
        void prepareForBattle();
        /* update the current position of the creature */
        void setPosition(float x, float y);
        /* update the current position of the creature */
        void move(float x, float y);
        /* update the future position of the creature */
        void moveFuture(float x, float y);
        /* affect the future position to the current position */
        void assignFuture2CurrentPos();
        /* affect the current position to the future position */
        void assignCurrent2FuturePos();
        /* browse the flowchart AI to get all actions that has to be executed */
        std::vector<Action*> * getActions(Battle const& simulation);
        /* reduce HP of damage (HP stay >= 0) */
        void takeDamage(float damage);

        // accessors
        std::vector<Actuator> const& getActuators() const { return _actuators;};
        Actuator const& getActuator(int idActuator) const {
                                        return _actuators[idActuator]; };
        Position const& getPosition() const { return _pos; };
        int getGlobalID() const { return _globalID; };
        float getRadius() const { return _radius; };
        float getHP() const { return _HP; };
        float getHPMax() const { return _HPMax; };
        float getEnergy() const { return _energy; };
        float getEnergyMax() const { return _energyMax; };

    private:
        int _globalID;
        static int _nbCreatureCreated;

        const float _radius;
        float _HP;
        static constexpr float _HPMax = 100.0f;
        float _energy;
        static constexpr float _energyMax = 100.0f;

        AI* _ai;
        std::vector<Actuator> _actuators;
        Position _pos;
        Position _futurePos;
};

#endif //CREATURE_H_INCLUDED

