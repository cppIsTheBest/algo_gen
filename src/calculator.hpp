#ifndef CALCULATOR_H_INCLUDED
#define CALCULATOR_H_INCLUDED

#include "Creature.hpp"

namespace calculator {
    /* return the creature's actuator the closest to other,
     * or -1 in case of error */
    int getIDClosestActuator(Creature const& creature, Creature const& other);

    /* return the creature's actuator the farthest to other,
     * or -1 in case of error */
    int getIDFarthestActuator(Creature const& creature, Creature const& other);

    /* check if a shot from the given actuator of the creature, and with the
     * given range will reach the other creature */
    bool lineIntersection(Creature const& creature, Creature const& other,
                          int idActuator, float range);
}

#endif //CALCULATOR_H_INCLUDED

