#ifndef CONDITIONNODE_H_INCLUDED
#define CONDITIONNODE_H_INCLUDED

#include <memory>
#include "Node.hpp"
#include "Condition.hpp"

/** A conditionnal node of the flowchart of a creature
 */
class ConditionNode : public Node {
    public:
        ConditionNode(Condition* condition, Node* ifNode);

        virtual void updateActions(std::vector<Action*> & actions,
                                   Battle const& simulation);
        virtual void randomUpdate();
        virtual void insertNode(Node* n);
        virtual int countNodes();
        virtual std::unique_ptr<Node> * getNodeDepthFirstSearch(
                                    int & currentNumber, int nodeNumber);
        virtual std::string toString(int indentLevel);

        void setIfNode(Node* n);
        void setElseNode(Node* n);

    private:
        std::unique_ptr<Condition> _condition;

        std::unique_ptr<Node> _ifNode;
        std::unique_ptr<Node> _elseNode;
};

#endif //CONDITIONNODE_H_INCLUDED

