#ifndef POSITION_H_INCLUDED
#define POSITION_H_INCLUDED

#include <SFML/Graphics.hpp>

#define PI 3.14159265

/** Defines the position and rotation of an object in a 2D environment
 */
class Position {
    public:
        Position();
        Position(float x, float y);

        void resetCoordinates(float x, float y);
        void move(float x, float y);

        //returns an angle between PI and -PI
        float getAngleWith(Position const& pos) const;
        sf::Vector2f getVector2f() const;
        float distance(Position const& position) const;

        // accessor
        float getX() const { return _x; };
        float getY() const { return _y; };

    private:
        float _x, _y;
};

Position operator+(Position const& a, Position const& b);

#endif //POSITION_H_INCLUDED

