#ifndef Battle_H_INCLUDED
#define Battle_H_INCLUDED

#include <iostream>
#include <vector>
#include <memory>
#include <SFML/Graphics.hpp>
#include "Creature.hpp"
#include "ViewManager.hpp"

/** can execute a fight between 2 creatures
 */
class Battle {
    public:
        Battle();
        void init(Creature* creature1, Creature* creature2);
        void iterate(ViewManager & viewManager);

        bool fightEnded() const;

        Creature const& getCreature1() const { return *_creature1; };
        Creature const& getCreature2() const { return *_creature2; };

    private:
        void act();
        void resolveActions(ViewManager & viewManager);

        sf::Clock _clock;

        std::vector<Action*> * _actions1;
        std::vector<Action*> * _actions2;
        Creature* _creature1;
        Creature* _creature2;

        int _nbIterations;
        int _nbIterationsMax;
};

#endif //Battle_H_INCLUDED

