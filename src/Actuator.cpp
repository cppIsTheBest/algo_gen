#include <cmath>
#include "Actuator.hpp"
#include "Creature.hpp"

Actuator::Actuator(float angle2Creature, Creature const& creature) :
    _angle2Creature(angle2Creature)
{
    resetCoordinatesAround(creature.getPosition().getX(),
                           creature.getPosition().getY(),
                           creature.getRadius());
}

void Actuator::resetCoordinatesAround(float xC, float yC, float radiusC) {
    float x( std::cos(_angle2Creature)*radiusC + xC);
    float y( std::sin(_angle2Creature)*radiusC + yC);
    _pos.resetCoordinates(x, y);
}

