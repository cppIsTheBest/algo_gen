#include <iostream>
#include "Population.hpp"

int main() {
    sf::ContextSettings guiSettings;
    guiSettings.antialiasingLevel = 8;
    Population population(100, guiSettings); 

    while (population.windowOpen()) {
        population.evaluation();
        population.creationOfNewCreatures();
        population.update();
    }

    std::cout << "[program end]" << std::endl;
    return 1;
}

