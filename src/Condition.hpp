#ifndef CONDITION_H_INCLUDED
#define CONDITION_H_INCLUDED

#include "Battle.hpp"
#include "Creature.hpp"

/** Abstract class which represent a condition that
 *  can be evaluated given the battle */
class Condition {
    public:
        virtual bool evaluate(Battle const& battle) const = 0;
        virtual void randomUpdate() = 0;
        virtual std::string toString(int indentLevel) = 0;
};

#endif //CONDITION_H_INCLUDED

