#include "AI.hpp"

AI::AI(Node* aiRoot) :
    _aiRoot(aiRoot)
{}

std::vector<Action*> & AI::getActions(Battle const& simulation) {
    _actions.clear();
    if (_aiRoot)
        _aiRoot->updateActions(_actions, simulation);

    return _actions;
}

std::string AI::toString() {
    if (_aiRoot)
        return _aiRoot->toString();
    else
        return std::string("Empty");
}

