#include "ActionAttack.hpp"
#include "views/LaserView.hpp"
#include "calculator.hpp"
#include "smallFcts.hpp"

ActionAttack::ActionAttack()
{}

void ActionAttack::act(ViewManager & viewManager, Creature & creature,
                                                  Creature & other) const {
    // get id of the closest actuator to the other creature
    int idActuator( calculator::getIDClosestActuator(creature, other) );
    if (idActuator >= 0
        && calculator::lineIntersection(creature, other, idActuator, _range)) {
        other.takeDamage(_damage);
        std::unique_ptr<View> view(new LaserView(creature, idActuator, _range,
                                                 1));
        viewManager.addView(std::move(view));
    }
}

void ActionAttack::randomUpdate() {
}

std::string ActionAttack::toString(int indentLevel) {
    return smallFcts::indent(indentLevel) + "ATTACK";
}

