#include "Battle.hpp"
#include "Creature.hpp"
#include "ActionApproach.hpp"
#include "calculator.hpp"
#include "smallFcts.hpp"

ActionApproach::ActionApproach()
{}

void ActionApproach::act(ViewManager & viewManager, Creature & creature,
                                                    Creature & other) const {
    // get the id of the most suitable actuator to approach the other creature
    int idActuator = calculator::getIDFarthestActuator(creature, other);
    if (idActuator >= 0) {
        std::vector<Actuator> actuators( creature.getActuators() );
        Position const& pos( creature.getPosition() );
        float angleC2Ac = pos.getAngleWith(actuators[idActuator].getPosition());
        float x(_offset*std::cos(angleC2Ac));
        float y(_offset*std::sin(angleC2Ac));
        // update the future position of the creature
        creature.moveFuture(x, y);
    }
}

void ActionApproach::randomUpdate() {
}

std::string ActionApproach::toString(int indentLevel) {
    return smallFcts::indent(indentLevel) + "APPROACH";
}

