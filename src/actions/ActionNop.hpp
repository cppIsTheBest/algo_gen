#ifndef ACTIONNOP_H_INCLUDED
#define ACTIONNOP_H_INCLUDED

#include "Action.hpp"

/** This action do nothing. */
class ActionNop : public Action {
    public:
        ActionNop();

        virtual void act(ViewManager & viewManager, Creature & creature,
                                                    Creature & other) const;
        virtual void randomUpdate();
        virtual std::string toString(int indentLevel);
}; 

#endif //ACTIONNOP_H_INCLUDED

