#include "Battle.hpp"
#include "Creature.hpp"
#include "ActionMoveAway.hpp"
#include "calculator.hpp"
#include "smallFcts.hpp"

ActionMoveAway::ActionMoveAway()
{}

void ActionMoveAway::act(ViewManager & viewManager, Creature & creature,
                                                    Creature & other) const {
    // get the id of the best actuator to move away from the other creature
    int idActuator = calculator::getIDClosestActuator(creature, other);
    if (idActuator >= 0) {
        std::vector<Actuator> actuators( creature.getActuators() );
        Position const& pos( creature.getPosition() );
        float angleC2Ac = pos.getAngleWith(actuators[idActuator].getPosition());
        float x(_offset*std::cos(angleC2Ac));
        float y(_offset*std::sin(angleC2Ac));
        // update the future position of the creature
        creature.moveFuture(x, y);
    }
}

void ActionMoveAway::randomUpdate() {
}

std::string ActionMoveAway::toString(int indentLevel) {
    return smallFcts::indent(indentLevel) + "MOVE AWAY";
}

