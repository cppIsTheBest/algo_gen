#include "ActionNop.hpp"
#include "smallFcts.hpp"

ActionNop::ActionNop()
{}

void ActionNop::act(ViewManager & viewManager, Creature & creature,
                                                    Creature & other) const {
    // do nothing. that's the point.
}

void ActionNop::randomUpdate() {
}

std::string ActionNop::toString(int indentLevel) {
    return smallFcts::indent(indentLevel) + "DO NOTHING\n";
}

