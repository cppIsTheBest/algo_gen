#ifndef ACTIONAPPROACH_H_INCLUDED
#define ACTIONAPPROACH_H_INCLUDED

#include "Action.hpp"

/** This action allow the creature to approach the other.
 *  the best oriented actuator is used
 *  (number of pixels defined by offset) */
class ActionApproach : public Action {
    public:
        ActionApproach();

        virtual void act(ViewManager & viewManager, Creature & creature,
                                                     Creature & other) const;
        virtual void randomUpdate();
        virtual std::string toString(int indentLevel);

    private:
        static constexpr float _offset = 0.8f; 
};

#endif //ACTIONAPPROACH_H_INCLUDED

