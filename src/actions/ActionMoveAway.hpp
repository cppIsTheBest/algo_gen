#ifndef ACTIONMOVEAWAY_H_INCLUDED
#define ACTIONMOVEAWAY_H_INCLUDED

#include "Action.hpp"

/** This action allow the creature to move away from the other.
 *  the best oriented actuator is used
 *  (number of pixels defined by offset) */
class ActionMoveAway : public Action {
    public:
        ActionMoveAway();

        virtual void act(ViewManager & viewManager, Creature & creature,
                                                     Creature & other) const;
        virtual void randomUpdate();
        virtual std::string toString(int indentLevel);

    private:
        static constexpr float _offset = 0.8f; 
};

#endif //ACTIONMOVEAWAY_H_INCLUDED

