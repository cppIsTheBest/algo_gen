#ifndef ACTIONATTACK_H_INCLUDED
#define ACTIONATTACK_H_INCLUDED

#include "Action.hpp"

/** This action allow the creature to attack with the actuator the clothest
 *  to the other */
class ActionAttack : public Action {
    public:
        ActionAttack();

        virtual void act(ViewManager & viewManager, Creature & creature,
                                                    Creature & other) const;
        virtual void randomUpdate();
        virtual std::string toString(int indentLevel);

    protected:
        static constexpr float _range = 60.0f;
        static constexpr float _damage = 1.0f;
}; 

#endif //ACTIONATTACK_H_INCLUDED

