#ifndef POPULATION_H_INCLUDED
#define POPULATION_H_INCLUDED

#include <list>
#include "Battle.hpp"
#include "GUI.hpp"
#include "Ranks.hpp"

/** A population is a set of creatures. The class provides functions which
 *  allow to do the classical operations of genetics algorithms on the AIs
 *  of these creatures */
class Population {
    public:
        // size is the number of creatures in the population
        Population(int size,
                sf::ContextSettings const& guiSettings=sf::ContextSettings());
        ~Population();

        /* evaluate the efficiency of AIs by making the creatures fight in
           1v1 battles */
        void evaluation();
        /* creates new creatures by introducing new mutations on the AIs of the
         * best ones */
        void creationOfNewCreatures();
        /* update the population by removing some individuals and adding the new
         * ones */
        void update();

        bool windowOpen();
        int getNbGenerations() const { return _nbGenerations; };
        int getNbBattles() const { return _nbBattles; };

    private:
        bool equal(std::list<Creature*>::iterator & it,
                   std::list<Creature*>::reverse_iterator & rit);

        std::list<Creature*> _creatures;
        Ranks _ranks;
        // the object wich will runs all the battles
        Battle _battle;
        GUI _gui;

        // number of generations created
        int _nbGenerations;
        // number of battles launched in the current generation
        int _nbBattles;
};

#endif //POPULATION_H_INCLUDED

