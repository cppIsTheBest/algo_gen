#include "GUI.hpp"
#include "Population.hpp"

GUI::GUI(Population const& population, Battle const& battle,
         sf::VideoMode videoMode, sf::String const& title, int style,
         sf::ContextSettings const& settings) :
    sf::RenderWindow(videoMode, title, style, settings),
    _population(population),
    _battle(battle)
{
    //load font
    std::string fontPath("Configuration/Fonts/DejaVuSans.ttf");
    if (!_font.loadFromFile(fontPath)) {
        std::cerr << "ERROR : impossible to load the font " << fontPath
                  << std::endl;
    }

    //set up texts
    _textNbGenerations.setFont(_font);
    _textNbBattles.setFont(_font);
    _textNbGenerations.setColor(sf::Color(sf::Color::Black));
    _textNbBattles.setColor(sf::Color(sf::Color::Black));
}

void GUI::prepareForBattle(Battle const& battle) {
    _creatureView1.setCreature(&battle.getCreature1()); 
    _creatureView2.setCreature(&battle.getCreature2()); 
}

void GUI::display() {
    if (_clock.getElapsedTime() >= sf::milliseconds(16)) {
        _clock.restart();
        clear(sf::Color::White);

        _creatureView1.display(*this);
        _creatureView2.display(*this);
        _viewManager.display(*this);

        // draw text showing current number of generations
        std::string intStr( std::to_string(_population.getNbGenerations()) );
        _textNbGenerations.setString("Generation " + intStr);
        sf::FloatRect rect( _textNbGenerations.getLocalBounds() );
        sf::Vector2u size( this->getSize() );
        _textNbGenerations.setPosition(size.x - rect.width - rect.left,
                                       size.y - rect.height - rect.top);
        draw(_textNbGenerations);

        // draw text showing current number of battles
        intStr = std::to_string(_population.getNbBattles());
        _textNbBattles.setString("Battle " + intStr);
        rect = _textNbBattles.getLocalBounds();
        _textNbBattles.setPosition(-rect.left, size.y - rect.height - rect.top);
        draw(_textNbBattles);

        RenderWindow::display();
    }
}

