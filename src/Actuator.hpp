#ifndef ACTUATOR_H_INCLUDED
#define ACTUATOR_H_INCLUDED

#include "Position.hpp"
class Creature;

/** Represents an actuator, an object wich allow a creature to act
 *  (move, attack, defend...)
 */
class Actuator {
    public:
        Actuator(float angle2Creature, Creature const& creature);

        void resetCoordinatesAround(float xC, float yC, float radius);

        float getAngle2Creature() const { return _angle2Creature; };
        Position const& getPosition() const { return _pos; };

    private:
        float _angle2Creature;
        Position _pos;
};

#endif //ACTUATOR_H_INCLUDED

