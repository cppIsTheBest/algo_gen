#include "LaserView.hpp"

LaserView::LaserView(Creature const& creature, int idActuator, float length,
                     int nbFramesTotal) :
    View(nbFramesTotal),
    _creature(creature),
    _idActuator(idActuator),
    _relative2CreaturePos(0, 0)
{
    sf::Vector2f size(length, 5);
    _shape.setSize(size);
    _shape.setFillColor(sf::Color(40, 40, 255));
    float angle2Creature(creature.getActuator(idActuator).getAngle2Creature());
    _shape.setRotation(angle2Creature*360/(2*PI));
}

void LaserView::display(sf::RenderWindow & window) {
    Position pos = _creature.getActuator(_idActuator).getPosition();
    pos = pos + _relative2CreaturePos;
    _shape.setPosition(pos.getVector2f());
    window.draw(_shape);
}

