#ifndef LASERVIEW_H_INCLUDED
#define LASERVIEW_H_INCLUDED

#include "View.hpp"
#include "Position.hpp"
#include "Creature.hpp"

/** A LaserView appears when a creature attacks. */
class LaserView : public View {
    public:
        // the parameter is the nb of frames where the laser should appear
        LaserView(Creature const& creature, int idActuator, float length,
                  int nbFramesTotal);
        virtual void display(sf::RenderWindow & window);

    private:
        // the creature associated to the LaserView
        Creature const& _creature;
        int _idActuator;
        // the position of the LaserView relative to the creature
        Position _relative2CreaturePos;
        sf::RectangleShape _shape;
};

#endif //LASERVIEW_H_INCLUDED

