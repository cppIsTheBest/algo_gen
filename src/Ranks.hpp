#ifndef RANKS_H_INCLUDED
#define RANKS_H_INCLUDED

#include <map>

/** can register informations about the creatures, and associate to each one
 * a rank (wich can then be used for the selection process) */
class Ranks {
    public:
        Ranks();
        // delete all stored data
        void reset();
        // update victory count for this ID
        void addVictory(int globalID, float HPLeft);
        // update HPLeft count for this ID
        void addHPLeft(int globalID, float HPLeft);
        /* assuming info has been provided via addVictory and addDefeat,
         * compute all creature's ranks */
        void computeAllRanks();
        /* if there is a creature with this rank, returns its global ID,
         * else returns -1 */
        int getIDFromRank(int rank);
        /* assuming that computeAllRanks has been called. returns -1 if no ranks
         * information stored */
        int getMaxRank();

    private:
        /* returns the score of an ID, using the stored information
         * (victories and others) */
        int computeScore(int globalID);

        /* associate to each creature's global ID a number of victories */
        std::map<int, int> _victories;
        /* associate to each creature's global ID the sum of its HP left
         * after each battle */
        std::map<int, int> _HPLeft;
        /* associate to each creature's global ID a rank
         * (from 1 to number of creatures, 1 being the best one) */
        std::map<int, int> _ranks;

};

#endif //RANKS_H_INCLUDED

