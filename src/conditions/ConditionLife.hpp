#ifndef CONDITIONLIFE_H_INCLUDED
#define CONDITIONLIFE_H_INCLUDED

#include "Condition.hpp"
#include "Enums.hpp"

/** this condition allow to check if it's life or the life of the
 *  other creature is inferior or superior to a certain value
 */
class ConditionLife : public Condition {
    public:
        /* constructor's arguments :
         *   - op : operator of the comparison
         *   - ident : indicates from which creature we check the life (1 or 2)
         *   - value : value of the comparison */
        ConditionLife(Operator op, int ident, float value);
        virtual bool evaluate(Battle const& battle) const;
        virtual void randomUpdate();
        virtual std::string toString(int indentLevel);

    private:
        Operator _op;
        int _ident;
        float _value;
};

#endif //CONDITIONLIFE_H_INCLUDED

