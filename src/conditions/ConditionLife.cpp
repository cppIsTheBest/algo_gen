#include "ConditionLife.hpp"
#include "smallFcts.hpp"

ConditionLife::ConditionLife(Operator op, int ident, float value) :
    _op(op),
    _ident(ident),
    _value(value)
{}

bool ConditionLife::evaluate(Battle const& battle) const {
    //TODO
    // there is a problem with the actual implementation. I want to check life
    // of this creature or the other, not really do the following
    Creature const& creature( _ident == 1 ?
                    battle.getCreature1() : battle.getCreature2() );

    switch (_op) {
        case SUPERIOR:
            if (creature.getHP() > _value)
                return true;
            break;
        case INFERIOR:
            if (creature.getHP() < _value)
                return true;
            break;
        default:
            return false;
            break;
    }
    return false;
}

void ConditionLife::randomUpdate() {
    int r = rand() % 100;

    if (r < 60) {
        /* 60% chances to change value. We selecting randomly between
         * (value / 2) and (value * 2) */
        float newValue;
        if (_value != 0) {
            newValue = (float((rand() % int(1000.0*_value*1.5)))
                        + 1000.0*_value*0.5) / 1000.0;
        }
        else
            newValue = rand() % 100;  // avoid modulo error
        _value = newValue;
    } else if (r < 80) {
        /* 20 % chances to change the identity of the checked creature */
        if (_ident == 1)
            _ident = 2;
        else
            _ident = 1;
    } else {
        /* 20 % chances to change the value of the comparison */
        if (_op == SUPERIOR)
            _op = INFERIOR;
        else
            _op = SUPERIOR;
    }
}

std::string ConditionLife::toString(int indentLevel) {
    std::string str = smallFcts::indent(indentLevel) + "Life of ";
    switch(_ident) {
        case 1:
            str += "creature 1 ";
            break;
        default:
            str += "creature 2 ";
            break;
    }
    switch(_op) {
        case SUPERIOR:
            str += ">";
            break;
        case INFERIOR:
            str += "<";
            break;
        case UNINITIALIZED:
            str += "OPERATOR_UNINITIALIZED";
            break;
        default:
            str += "STR_UNDEFINED_FOR_THIS_OPERATOR";
            break;
    }
    str += " ";
    str += std::string(ITOSTR(_value));
    return str;
}

