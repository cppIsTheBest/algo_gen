#ifndef CONDITIONDISTANCE_H_INCLUDED
#define CONDITIONDISTANCE_H_INCLUDED

#include "Condition.hpp"
#include "Enums.hpp"

/** this condition allow to check if the distance between the two creatures is
 *  inferior or superior to a certain value
 */
class ConditionDistance : public Condition {
    public:
        ConditionDistance();
        ConditionDistance(Operator op, float value);
        virtual bool evaluate(Battle const& battle) const;
        virtual void randomUpdate();
        virtual std::string toString(int indentLevel);

    private:
        Operator _op;
        float _value;
};

#endif //CONDITIONDISTANCE_H_INCLUDED

