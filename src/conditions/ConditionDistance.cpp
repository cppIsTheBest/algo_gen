#include "ConditionDistance.hpp"
#include "smallFcts.hpp"

ConditionDistance::ConditionDistance(Operator op, float value) :
    _op(op),
    _value(value)
{}

ConditionDistance::ConditionDistance() :
    _op(UNINITIALIZED)
{}

bool ConditionDistance::evaluate(Battle const& battle) const {
    Creature const& creature1( battle.getCreature1() );
    Creature const& creature2( battle.getCreature2() );

    switch (_op) {
        case SUPERIOR:
            if (creature1.getPosition().distance(creature2.getPosition())
                > _value)
                return true;
            break;
        case INFERIOR:
            if (creature1.getPosition().distance(creature2.getPosition())
                < _value)
                return true;
            break;
        default:
            return false;
            break;
    }
    return false;
}

void ConditionDistance::randomUpdate() {
    int r = rand() % 100;

    if (r < 80) {
        /* 80% chances to change value. We selecting randomly between
         * (value / 2) and (value * 2) */
        float newValue( (float((rand() % int(1000.0*_value*1.5)))
                        + 1000.0*_value*0.5) / 1000.0 );
        _value = newValue;
    } else {
        /* 20 % chances to change the value of the comparison */
        if (_op == SUPERIOR)
            _op = INFERIOR;
        else
            _op = SUPERIOR;
    }
}

std::string ConditionDistance::toString(int indentLevel) {
    std::string str = smallFcts::indent(indentLevel) + "Distance ";
    switch(_op) {
        case SUPERIOR:
            str += ">";
            break;
        case INFERIOR:
            str += "<";
            break;
        case UNINITIALIZED:
            str += "OPERATOR_UNINITIALIZED";
            break;
        default:
            str += "STR_UNDEFINED_FOR_THIS_OPERATOR";
            break;
    }
    str += " ";
    str += std::string(ITOSTR(_value));
    return str;
}

