#include <stdlib.h>
#include <time.h>
#include "aiModifier.hpp"
#include "Condition.hpp"
#include "ConditionNode.hpp"
#include "ActionNode.hpp"
#include "actions/ActionApproach.hpp"
#include "actions/ActionAttack.hpp"
#include "actions/ActionMoveAway.hpp"
#include "conditions/ConditionDistance.hpp"
#include "conditions/ConditionLife.hpp"
#include "Enums.hpp"



void aiModifier::randomMutation(AI & ai) {
    // random selection of the type of mutation which will be applied
    int r( rand() % 100 );

    if (r < 10) {
        // 10 % chances of node deletion
        randomNodeDeletion(ai);
    } else if (r < 55) {
        // 45 % chances of node addition
        randomNodeAddition(ai);
    } else {
        // 45 % chances of node update
        randomNodeUpdate(ai);
    }
}

void aiModifier::randomNodeDeletion(AI & ai) {
}

void aiModifier::randomNodeAddition(AI & ai) {
    int nodeNumber( rand() % countNodes(ai) );
    std::unique_ptr<Node> * node( getNodeDepthFirstSearch(ai, nodeNumber) );
    (*node)->insertNode(createRandomNode());
}

void aiModifier::randomNodeUpdate(AI & ai) {
    int nodeNumber( rand() % countNodes(ai) );
    std::unique_ptr<Node> * node( getNodeDepthFirstSearch(ai, nodeNumber) );
    (*node)->randomUpdate();
}

Node* aiModifier::createRandomNode() {
    Node* n;

    // choose between a ConditionNode or an ActionNode
    if (rand() % 2 == 0) {
        Operator op = (rand() % 2 == 0) ? SUPERIOR : INFERIOR;
        Condition* c;
        // choose between a ConditionDistance or a ConditionLife
        if (rand() % 2 == 0) {
            float distance = rand() % 1000;
            c = new ConditionDistance(op, distance);
        }
        else {
            float life = rand() % 100;
            int ident = rand() % 2;
            c = new ConditionLife(op, ident, life);
        }
        n = new ConditionNode(c, nullptr);
    } else {
        Action* a;
        // choose between one of the existing actions types
        switch (rand() % 3) {
            case 0:
                a = new ActionApproach();
                break;
            case 1:
                a = new ActionMoveAway();
                break;
            case 2:
                a = new ActionAttack();
                break;
            default:
                a = nullptr;
                break;
        }
        n = new ActionNode(a);
    }
    return n;
}

int aiModifier::countNodes(AI & ai) {
    return ai.getRoot()->countNodes() + 1;
}

std::unique_ptr<Node> * aiModifier::getNodeDepthFirstSearch(AI & ai,
                                                            int nodeNumber) {
    if (nodeNumber == 0) {
        return &(ai.getRoot());
    } else {
        int currentNumber(0);
        return ai.getRoot()->getNodeDepthFirstSearch(currentNumber, nodeNumber);
    }
}

