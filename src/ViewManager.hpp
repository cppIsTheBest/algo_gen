#ifndef VIEWMANAGER_H_INCLUDED
#define VIEWMANAGER_H_INCLUDED

#include <vector>
#include "View.hpp"

/** Manages the display of all view (for example the ones that result from
 *  actions, like the the shield for the protection action).
 *  Display each view for a certain number of frames, then remove it */
class ViewManager {
    public:
        ViewManager();
        void addView(std::unique_ptr<View> view);
        // display all the views and decrease their number of frames
        void display(sf::RenderWindow & window);

    private:
        std::vector< std::unique_ptr<View> > _views;
};

#endif //VIEWMANAGER_H_INCLUDED

