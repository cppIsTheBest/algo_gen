#include <iostream>
#include <cmath>
#include "Position.hpp"

Position::Position() :
    _x(0.0), _y(0.0)
{}

Position::Position(float x, float y) :
    _x(x), _y(y)
{}

void Position::Position::resetCoordinates(float x, float y) {
    _x = x;
    _y = y;
}

void Position::move(float x, float y) {
    _x += x;
    _y += y;
}

float Position::getAngleWith(Position const& pos) const {
    float offsetX( _x - pos.getX() );
    float offsetY( _y - pos.getY() );
    float l( std::sqrt(std::pow(offsetX,2) + std::pow(offsetY,2)) );
    if (offsetY > 0) {
        return std::acos(offsetX/l);
    } else {
        return 2*PI - std::acos(offsetX/l);
    }
}

sf::Vector2f Position::getVector2f() const {
    return sf::Vector2f(_x, _y);
}

float Position::distance(Position const& position) const {
    return std::sqrt(std::pow(position.getX()-_x, 2)
                     + std::pow(position.getY()-_y, 2));
}

Position operator+(Position const& a, Position const& b) {
    Position pos(a.getX() + b.getX(), a.getY() + b.getY());
    return pos;
}
