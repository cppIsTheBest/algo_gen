#ifndef ACTIONNODE_H_INCLUDED
#define ACTIONNODE_H_INCLUDED

#include <memory>
#include "Node.hpp"

/* ActionNode is a node in the flowchart of a creature
 * which contains an action */
class ActionNode : public Node {
    public:
        ActionNode(Action* action);
        ~ActionNode();

        virtual void updateActions(std::vector<Action*> & actions,
                                   Battle const& simulation);
        virtual void randomUpdate();
        virtual void insertNode(Node* n);
        virtual int countNodes();
        virtual std::unique_ptr<Node> * getNodeDepthFirstSearch(
                                    int & currentNumber, int nodeNumber);
        virtual std::string toString(int indentLevel);

        void setNext(Node* n);

    private:
        Action* _action;
        std::unique_ptr<Node> _nextNode;
};

#endif //ACTIONNODE_H_INCLUDED

