#ifndef CREATURE_VIEW_INCLUDED
#define CREATURE_VIEW_INCLUDED

#include <memory>
#include <SFML/Graphics.hpp>
//#include "Creature.hpp"
class Creature;

/** defines the view of a creature, to allow it to be displayed
 */
class CreatureView {
    public:
        CreatureView();
        void setCreature(const Creature* creature);
        void display(sf::RenderWindow & window);

    private:
        const Creature* _creature;
        // creature body's view
        sf::CircleShape _shape;
        // actuator's views
        std::vector<sf::CircleShape> _actuatorShapes;

        // life bar's view
        sf::RectangleShape _lifeBar;
        // life bar's background
        sf::RectangleShape _lifeBarBG;  
        // energy bar's view
        sf::RectangleShape _energyBar;
        // energy bar's background
        sf::RectangleShape _energyBarBG;
        float _barsLengthX;
        float _barsLengthY;

        float _actuatorsRadius;
};

#endif //CREATURE_VIEW_INCLUDED

