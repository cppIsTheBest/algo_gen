#ifndef ACTION_H_INCLUDED
#define ACTION_H_INCLUDED

#include "ViewManager.hpp"
class Creature;

/** Abstract class which defines an action that can be done
 *  by a creature during an iteration */
class Action {
    public:
        virtual ~Action() {};
        //creature is the creature which will act
        virtual void act(ViewManager & viewManager, Creature & creature,
                                                    Creature & other) const = 0;
        virtual void randomUpdate() = 0;
        virtual std::string toString(int indentLevel) = 0;
}; 

#endif //ACTION_H_INCLUDED

