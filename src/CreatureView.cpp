#include "CreatureView.hpp"
#include "Creature.hpp"
#include <iostream>

CreatureView::CreatureView() :
    _actuatorsRadius(5)
{
    _shape.setFillColor(sf::Color(150, 150, 150));
}

void CreatureView::setCreature(const Creature* creature) {
    _creature = creature;
    _shape.setRadius(_creature->getRadius());
    _barsLengthX = 0.4*_creature->getRadius();
    _barsLengthY = 1.15*_creature->getRadius();

    // actuator's views set-up
    _actuatorShapes.clear();
    std::vector<Actuator> const& actuators( _creature->getActuators() );
    for (unsigned int i(0); i<actuators.size(); i++) {
        _actuatorShapes.push_back( sf::CircleShape(_actuatorsRadius) );
        _actuatorShapes[i].setFillColor(sf::Color(50, 50, 50));
    }

    // life bar set-up
    sf::Vector2f size(_barsLengthX, _barsLengthY);
    _lifeBar.setSize(size);
    _lifeBar.setFillColor(sf::Color::Red);
    _lifeBarBG.setSize(size);
    _lifeBarBG.setFillColor(sf::Color::Black);

    // energy bar set-up
    _energyBar.setSize(size);
    _energyBar.setFillColor(sf::Color::Green);
    _energyBarBG.setSize(size);
    _energyBarBG.setFillColor(sf::Color::Black);

}

void CreatureView::display(sf::RenderWindow & window) {
    if (_creature != nullptr) {
        // draw creature shape
        sf::Vector2f pos = _creature->getPosition().getVector2f();
        pos.x -= _shape.getRadius();
        pos.y -= _shape.getRadius();
        _shape.setPosition(pos);
        window.draw(_shape);

        // draw creature's actuator's shapes
        for (unsigned int i(0); i<_actuatorShapes.size(); i++) {
            pos = _creature->getActuators()[i].getPosition().getVector2f();
            pos.x -= _actuatorShapes[i].getRadius();
            pos.y -= _actuatorShapes[i].getRadius();
            _actuatorShapes[i].setPosition(pos);
            window.draw(_actuatorShapes[i]);
        }

        // draw life bar's shapes
        float hp( _creature->getHP() );
        float hpMax( _creature->getHPMax() );
        pos = _creature->getPosition().getVector2f();
        pos.x -= _creature->getRadius()*0.5;
        pos.y -= _creature->getRadius()*0.57;
        _lifeBarBG.setPosition(pos);
        float sizeY( hp*_barsLengthY/hpMax );
        sf::Vector2f sizeVec;
        if (sizeY >= 0 && sizeY <= _barsLengthY) {
            sizeVec = sf::Vector2f(_barsLengthX, sizeY);
            pos.y += _barsLengthY - sizeY;
        } else {
            sizeVec = sf::Vector2f(_barsLengthX, 0);
        }
        _lifeBar.setSize(sizeVec);
        _lifeBar.setPosition(pos);
        window.draw(_lifeBarBG);
        window.draw(_lifeBar);

        // draw energy bar's shapes
        float energy( _creature->getEnergy() );
        float energyMax( _creature->getEnergyMax() );
        pos = _creature->getPosition().getVector2f();
        pos.x += _creature->getRadius()*0.25;
        pos.y -= _creature->getRadius()*0.57;
        _energyBarBG.setPosition(pos);
        sizeY =  energy*_barsLengthY/energyMax;
        if (sizeY >= 0 && sizeY <= _barsLengthY) {
            sizeVec = sf::Vector2f(_barsLengthX, sizeY);
            pos.y += _barsLengthY - sizeY;
        } else {
            sizeVec = sf::Vector2f(_barsLengthX, 0);
        }
        _energyBar.setSize(sizeVec);
        _energyBar.setPosition(pos);
        window.draw(_energyBarBG);
        window.draw(_energyBar);
    }
}

