#include "Ranks.hpp"

Ranks::Ranks()
{}

void Ranks::reset() {
    _victories.clear();
    _HPLeft.clear();
    _ranks.clear();
}

void Ranks::addVictory(int globalID, float HPLeft) {
    // add victory and HPLeft to the count
    if  (_victories.find(globalID) != _victories.end()) {
        _victories.at(globalID) ++;
        _HPLeft.at(globalID) += HPLeft;
    } else {
        _victories[globalID] = 1;
        if (_HPLeft.find(globalID) != _HPLeft.end())
            _HPLeft.at(globalID) += HPLeft;
        else
            _HPLeft[globalID] = HPLeft;
    }
}

void Ranks::addHPLeft(int globalID, float HPLeft) {
    // add HPLeft to the count
    if (_HPLeft.find(globalID) != _HPLeft.end())
        _HPLeft.at(globalID) += HPLeft;
    else
        _HPLeft[globalID] = HPLeft;
}

void Ranks::computeAllRanks() {
    for (auto it(_HPLeft.begin()); it != _HPLeft.end(); it++) {
        int ID( (*it).first );
        int score( computeScore(ID) );

        // search what rank will have this ID
        int scoreComp(-1);
        int rankComp(-1);
        for (auto itR(_ranks.begin()); itR != _ranks.end(); itR++) {
            int curScore( computeScore((*itR).first) );
            if (score > curScore && curScore > scoreComp) {
                scoreComp = curScore;
                rankComp = (*itR).second;
            }
        }

        /* if this ID has a higher score than an already ranked ID, we give it
         * the good rank and increase all superior ranks.
         * else we give it the maximum rank */
        if (rankComp != -1) {
            _ranks[ID] = rankComp;
            for (auto itR(_ranks.begin()); itR != _ranks.end(); itR++)
                if ((*it).second >= rankComp)
                    _ranks.at((*itR).first) ++;
        } else {
            _ranks[ID] = getMaxRank() + 1;
        }
    }
}

int Ranks::getIDFromRank(int rank) {
    for (auto it(_ranks.begin()); it != _ranks.end(); it++) {
        if ((*it).second == rank)
            return (*it).second;
    }
    return -1;
}

int Ranks::getMaxRank() {
    int rankMax( -1 );
    for (auto it(_ranks.begin()); it != _ranks.end(); it++) {
        int currentRank( (*it).second );
        if (currentRank > rankMax)
            rankMax = currentRank;
    }
            
    return rankMax;
}

int Ranks::computeScore(int globalID) {
    // we attach greater importance to victories than to HPLeft
    int score(0);
    if (_HPLeft.find(globalID) != _HPLeft.end())
        score += _HPLeft.at(globalID);
    if (_victories.find(globalID) != _victories.end())
        score += _victories.at(globalID)*1000000;

    return score;
}

