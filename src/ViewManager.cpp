#include "ViewManager.hpp"

ViewManager::ViewManager() {
}

void ViewManager::addView(std::unique_ptr<View> view) {
    _views.push_back(std::move(view));
}

void ViewManager::display(sf::RenderWindow & window) {
    // display all the views
    auto it = _views.begin();
    while (it != _views.end()) {
        // display the view
        (*it)->display(window);
        // if the view has been displayed enough, delete it
        if (!(*it)->decrFramesLeft())
            it = _views.erase(it);
        else
            it++;
    }
}

