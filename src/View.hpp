#ifndef VIEW_H_INCLUDED
#define VIEW_H_INCLUDED

#include <SFML/Graphics.hpp>

/** Defines a view element which must be displayed on a certain number of frames
 */
class View {
    public:
        virtual void display(sf::RenderWindow & window) = 0;
        // decrement _nbFramesLeft and return true if > 0
        bool decrFramesLeft();

    protected:
        View();
        // the param is the total number of frames when the view must be
        // displayed
        View(int nbFramesTotal);

        int _nbFramesLeft;
        static constexpr int _defaultNbFramesTotal = 1;
};

#endif //VIEW_H_INCLUDED

