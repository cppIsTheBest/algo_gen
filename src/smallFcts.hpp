#ifndef SMALL_FCTS_HPP
#define SMALL_FCT_HPP

#include <string>

#include <sstream>
//conversion from int to string
#define ITOSTR( x ) static_cast< std::ostringstream & >( \
        ( std::ostringstream() << std::dec << x ) ).str()

namespace smallFcts {
    /* returns indentation string corresponding to an indentation level */
    std::string indent(int indentLevel);
}

#endif //SMALL_FCTS_HPP

