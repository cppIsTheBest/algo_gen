#ifndef GUI_H_IINCLUDED
#define GUI_H_IINCLUDED

#include "CreatureView.hpp"
#include "ViewManager.hpp"
#include "Battle.hpp"
class Population;

/** Main graphic interface, manages the window and the display
 */
class GUI : public sf::RenderWindow {
    public:
        GUI(Population const& population, Battle const& battle,
            sf::VideoMode videoMode, sf::String const& title,
            int style=sf::Style::Default,
            sf::ContextSettings const& settings=sf::ContextSettings());
        void prepareForBattle(Battle const& battle);
        void display();

        ViewManager & getViewManager() { return _viewManager; } ;

    private:
        Population const& _population;
        Battle const& _battle;

        ViewManager _viewManager;
        CreatureView _creatureView1;
        CreatureView _creatureView2;

        sf::Font _font;
        // text containing the current number of generations
        sf::Text _textNbGenerations;
        // text containing the current number of battles
        sf::Text _textNbBattles;

        sf::Clock _clock;
};

#endif //GUI_H_IINCLUDED

