#include "View.hpp"

View::View() : _nbFramesLeft(_defaultNbFramesTotal) {
}

View::View(int nbFramesTotal) : _nbFramesLeft(nbFramesTotal) {
}

bool View::decrFramesLeft() {
    _nbFramesLeft --;
    if (_nbFramesLeft > 0)
        return true;
    else
        return false;
}

