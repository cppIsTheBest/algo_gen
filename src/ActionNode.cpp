#include <memory>
#include "ActionNode.hpp" 
#include "Battle.hpp"
#include "ConditionNode.hpp"

ActionNode::ActionNode(Action* action) : Node(),
    _action(action)
{}

ActionNode::~ActionNode() {
    delete _action;
}

void ActionNode::updateActions(std::vector<Action*> & actions,
                               Battle const& simulation) {
    actions.push_back(_action);
    if (_nextNode)
        _nextNode->updateActions(actions, simulation);
}

void ActionNode::randomUpdate() {
    _action->randomUpdate();
}

void ActionNode::insertNode(Node* n) {
    if (ConditionNode* cn = dynamic_cast<ConditionNode*>(n)) {
        cn->setIfNode(_nextNode.release());
        _nextNode.reset(n);
    }
    else if (ActionNode* an = dynamic_cast<ActionNode*>(n)) {
        an->setNext(_nextNode.release());
        _nextNode.reset(n);
    }
}

int ActionNode::countNodes() {
    if (_nextNode)
        return _nextNode->countNodes() + 1;
    else
        return 0;
}

std::unique_ptr<Node> * ActionNode::getNodeDepthFirstSearch(
                                 int & currentNumber, int nodeNumber) {
    if (_nextNode) {
        if (currentNumber + 1 == nodeNumber) {
            return &(_nextNode);
        } else {
            currentNumber ++;
            return _nextNode->getNodeDepthFirstSearch(
                                            currentNumber, nodeNumber);
        }
    } else {
        return nullptr;
    }
}

std::string ActionNode::toString(int indentLevel) {
    std::string str = _action->toString(indentLevel);
    if (_nextNode)
        str += "\n" + _nextNode->toString(indentLevel);
    return str;
}

void ActionNode::setNext(Node* n) {
    _nextNode.reset(n);
}

