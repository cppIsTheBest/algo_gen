#ifndef AIMODIFIER_H_INCLUDED
#define AIMODIFIER_H_INCLUDED

#include "AI.hpp"

namespace aiModifier {
    void randomMutation(AI & ai);

    void randomNodeDeletion(AI & ai);
    void randomNodeAddition(AI & ai);
    void randomNodeUpdate(AI & ai);
    Node* createRandomNode();

    // returns the number of nodes of the given ai
    int countNodes(AI & ai);
    // returns the node number "nodeNumber", counting nodes as in a depth first
    // search
    std::unique_ptr<Node> * getNodeDepthFirstSearch(AI & ai, int nodeNumber);
}

#endif //AIMODIFIER_H_INCLUDED

