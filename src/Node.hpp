#ifndef NODE_H_INCLUDED
#define NODE_H_INCLUDED

#include <memory>
#include <vector>
#include "Action.hpp"
class Battle;

/* Abstract class which represents a node in the
 * flowchart of a creature */
class Node {
    public:
        virtual ~Node();
        virtual void updateActions(std::vector<Action*> & actions,
                                   Battle const& simulation) = 0;
        virtual void randomUpdate() = 0;
        /* insert the node given in parameter after this node in the flowchart
         */
        virtual void insertNode(Node* n) = 0;
        /* returns all the children of the node recursively (does not count
         * itself) */
        virtual int countNodes() = 0;
        /* returns the node number "nodeNumber", doing a depth first search,
         * knowing this node is the number "currentNumber",
         * assuming that currentNumber != nodeNumber
         * returns nullptr if the node doesn't exist */
        virtual std::unique_ptr<Node> * getNodeDepthFirstSearch(
                                    int & currentNumber, int nodeNumber) = 0;
        /* returns the node (and its children) as string */
        std::string toString();
        /* returns the node (and its children) as string, adding indentation
         * corresponding to current indent level */
        virtual std::string toString(int indentLevel) = 0;

    protected:
        Node();
};

#endif //NODE_H_INCLUDED

