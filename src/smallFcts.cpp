#include "smallFcts.hpp"

std::string smallFcts::indent(int indentLevel) {
    std::string s;
    for (int i(0); i<indentLevel; i++)
        s += "    ";
    return s;
}

