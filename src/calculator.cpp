#include "calculator.hpp"

int calculator::getIDClosestActuator(
                Creature const& creature, Creature const& other) {
    std::vector<Actuator> actuators( creature.getActuators() );
    Position const& posOther( other.getPosition() );

    if (actuators.size() >= 0) {
        int indexMin(0);
        float min(actuators[0].getPosition().distance(posOther));
        float distance;
        for (unsigned int i(0); i<actuators.size(); i++) {
            distance = actuators[i].getPosition().distance(posOther);
            if (distance < min) {
                min = distance;
                indexMin = i;
            }
        }
        return indexMin;
    }
    else {
        return -1;
    }
}

int calculator::getIDFarthestActuator(
                Creature const& creature, Creature const& other) {
/*  -- the following code works but it's unecessary complicated because it uses
       angles --
    // search for the actuator the farthest to the other creature
    int indexMax(-1);
    float max(-1);
    float angleO2C( other.getPosition().getAngleWith(_pos) );
    float angleAc2C;
    float z;
    for (unsigned int i(0); i<_actuators.size(); i++) {
        angleAc2C = _actuators[i].getPosition().getAngleWith(_pos);
        //absolute difference between the 2 angles
        z = std::abs(PI - std::abs(PI - std::abs(angleO2C-angleAc2C)));
        if (z > max) {
            max = z;
            indexMax = i;
        }
    }*/
    std::vector<Actuator> actuators( creature.getActuators() );
    Position const& posOther( other.getPosition() );

    int indexMax(-1);
    float max(-1);
    float distance;
    for (unsigned int i(0); i<actuators.size(); i++) {
        distance = actuators[i].getPosition().distance(posOther);
        if (distance > max) {
            max = distance;
            indexMax = i;
        }
    }

    return indexMax;
}

bool calculator::lineIntersection(Creature const& creature,
                        Creature const& other, int idActuator, float range) {
    return true;
}

