# TODO
begin code architecture


# Genetic algorithms applied to AI of 2D fighting creatures

## INTRODUCTION
2D creatures will learn to fight (1vs1) in a 2D environment with the help of
genetic algorithms.
For each iteration of a fight :
* each creature will follow its AI and establish a list of actions it wants to
  perform for this iteration
* a system will takes care of the execution of the valid actions, and if
  necessary 

## CREATURES
Each creature has HP and an energy quantity. When the HP reache 0, the
creature lose the fight. A tiny quantity of energy is gained at each iteration.

## AIs
An AI is a flow chart with condition and actions nodes. At each iteration of
the fight, it runs entirely and establish a list of actions to perform.

#### actions
Each action consume energy.
* ROTATE : rotate of a fixed angle, in one of the two directions.
* MOVE : move in the opposite direction of ones of the actuators (fixed value).
* ATTACK : attack with one of the actuators, the length is variable and impact
  the energy consumption.
* SHIELD : generate a shield in front of one of the actuators.

#### conditions
The following informations can be accessed by the AI :
* POSITION relative to the fight area
* ROTATION relative to the fight area
* POSITION relative to the ennemy
* ANGLE between the closest actuator of the creature and the ennemy
* ANGLE between the creature and the closest actuator of the ennemy
* ENERGY left
* HP left


## GENETIC ALGORITHMS

## DESIGN IDEAS

3 or 4 actuators by creature

#### colors

* white / very light grey background
* grey creatures
* red when creature touched
* oranges / yellow shots
* blue deplacement shots and shield

